package com.fajar.testBackend.constant;

public class ServiceHelper {

	public static final String CONST__PARAM_ID 									= "id";
	public static final String CONST__PARAM_NAME 									= "name";
	public static final String CONST__PARAM_USER_ID 								= "userId";
	
	public static final String CONST__PATH_USER_GET_ALL 					= "/all";
	public static final String CONST__PATH_USER_GET_BY_ID 					= "/getById";
	public static final String CONST__PATH_USER_GET_BY_NAME 				= "/getByName";
	public static final String CONST__PATH_USER_CONTACT_GET_ALL 			= "/contact/all";
	public static final String CONST__PATH_USER_CONTACT_GET_BYE_ID 			= "/contact/getById";
	public static final String CONST__PATH_USER_CONTACT_GET_BYE_USER_ID 	= "/contact/getByUserId";
	
}
