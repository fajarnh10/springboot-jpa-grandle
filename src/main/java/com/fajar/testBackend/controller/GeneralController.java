package com.fajar.testBackend.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fajar.testBackend.constant.ServiceHelper;
import com.fajar.testBackend.model.User;
import com.fajar.testBackend.model.UserContact;
import com.fajar.testBackend.repository.UserContactRepository;
import com.fajar.testBackend.repository.UserRepository;

@RestController
@RequestMapping("/user")
public class GeneralController {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserContactRepository userContactRepository;
	
	@RequestMapping(value=ServiceHelper.CONST__PATH_USER_GET_ALL, method = RequestMethod.GET)
	public @ResponseBody List<User> findAll(){
		List<User> response = new ArrayList<User>();
		
		try {
			for(User user : userRepository.findAll()){
				User singleUser = new User();
				singleUser.setId(user.getId());
				singleUser.setName(user.getName());
				singleUser.setAge(user.getAge());
				
				response.add(singleUser);
			}
		} catch (Exception e) {
			System.out.println("Error when fetch all user :  " + e);
		}
		
		return response;
	}
	
	@RequestMapping(value=ServiceHelper.CONST__PATH_USER_GET_BY_ID, method = RequestMethod.GET)
	public 	@ResponseBody User findById(
			@RequestParam(ServiceHelper.CONST__PARAM_ID) String id){
		User response =  null;
		
		try {
			response = userRepository.findById(id);
		} catch (Exception e) {
			System.out.println("Error when fetch all user :  " + e);
		}
		
		return response;
	}
	
	@RequestMapping(value=ServiceHelper.CONST__PATH_USER_GET_BY_NAME, method = RequestMethod.GET)
	public 	@ResponseBody User findByName(
			@RequestParam(ServiceHelper.CONST__PARAM_NAME) String name){
		User response =  null;
		
		try {
			response = userRepository.findByName(name);
		} catch (Exception e) {
			System.out.println("Error when fetch all user :  " + e);
		}
		
		return response;
	}
	
	@RequestMapping(value=ServiceHelper.CONST__PATH_USER_CONTACT_GET_ALL, method = RequestMethod.GET)
	public @ResponseBody List<UserContact> findAllContact(){
		List<UserContact> response = new ArrayList<UserContact>();
		
		try {
			for(UserContact user : userContactRepository.findAll()){
				UserContact singleUserContact = new UserContact();
				singleUserContact.setId(user.getId());
				singleUserContact.setAddress(user.getAddress());
				singleUserContact.setUserId(user.getUserId());
				
				response.add(singleUserContact);
			}
		} catch (Exception e) {
			System.out.println("Error when fetch all user :  " + e);
		}
		
		return response;
	}
	
	@RequestMapping(value=ServiceHelper.CONST__PATH_USER_CONTACT_GET_BYE_ID, method = RequestMethod.GET)
	public 	@ResponseBody UserContact findContactById(
			@RequestParam(ServiceHelper.CONST__PARAM_ID) String id){
		UserContact response =  null;
		
		try {
			response = userContactRepository.findById(id);
		} catch (Exception e) {
			System.out.println("Error when fetch all user :  " + e);
		}
		
		return response;
	}
	
	@RequestMapping(value=ServiceHelper.CONST__PATH_USER_CONTACT_GET_BYE_USER_ID, method = RequestMethod.GET)
	public 	@ResponseBody UserContact findContactByUserId(
			@RequestParam(ServiceHelper.CONST__PARAM_USER_ID) String userId){
		UserContact response =  null;
		
		try {
			response = userContactRepository.findByUserId(userId);
		} catch (Exception e) {
			System.out.println("Error when fetch all user :  " + e);
		}
		
		return response;
	}
	
}
