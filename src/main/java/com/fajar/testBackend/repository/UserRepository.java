package com.fajar.testBackend.repository;

import org.springframework.data.repository.CrudRepository;

import com.fajar.testBackend.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

	User findById(String id);
	
	User findByName(String name);
	
}
