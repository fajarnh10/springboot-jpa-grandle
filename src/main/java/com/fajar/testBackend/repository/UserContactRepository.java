package com.fajar.testBackend.repository;

import org.springframework.data.repository.CrudRepository;

import com.fajar.testBackend.model.UserContact;

public interface UserContactRepository extends CrudRepository<UserContact, Long> {

	UserContact findById(String id);
	
	UserContact findByUserId(String userId);
	
}
